# BetterTeddit

Lightweight extensible userscript for Teddit instances for Reddit.

Changes to Teddit backend may break the script, please let me know if it does.

Access to javascript you can audit in advance. Turn auto-update off, audit before update.

On a Teddit instance if there were any javascript you would have no guarantee the host didn't modify any included javascript at random one day.

You have no guarantees about their backend in regards to tracking, but at least you won't be part of someone's mining botnet.

## Features

Most features can easily be toggled on or off.

- Expand Images toggle.
- Auto expand all.
- Image click and drag resize.
- Fixed navbar with navigation links.
- Choose favorite subs for navbar.
- Forced Anonymous
- Colorized User ID
- Hide Points
- Less branding
- Custom CSS import
